#!/bin/sh
./resize.py --width=100 origin.jpg resize_w100.jpg
./resize.py --width=100 --crop default origin.jpg resize_w100_crop.jpg
./resize.py --width=100 --crop whitespace origin.jpg resize_w100_crop_whitespace.jpg
./resize.py --height=100 origin.jpg resize_h100.jpg
./resize.py --height=100 --crop default origin.jpg resize_h100_crop.jpg
./resize.py --height=100 --crop whitespace origin.jpg resize_h100_crop_whitespace.jpg
./resize.py --width=100 --height=100 origin.jpg resize_w100_h100.jpg
./resize.py --width=100 --height=100 --crop default origin.jpg resize_w100_h100_crop.jpg
./resize.py --width=100 --height=100 --crop whitespace origin.jpg resize_w100_h100_crop_whitespace.jpg
./resize.py --enhance origin.jpg enhance.jpg