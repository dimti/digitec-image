#!/usr/bin/env python
from __future__ import division
import os, sys
from PIL import Image
from PIL import ImageEnhance
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("source", type=str)
parser.add_argument("watermark", type=str)
args = parser.parse_args()

infile = args.source
watermark = args.watermark
try:
    im = Image.open(infile)
    im_watermark = Image.open(watermark)
    im = im.convert()
    im.paste(im_watermark, (im.size[0] // 2 - im_watermark.size[0] // 2, im.size[1] // 2 - im_watermark.size[1] // 2), im_watermark)
    im.save(infile, "JPEG", quality=100)
except IOError:
    print("cannot create thumbnail for '%s'" % infile)
